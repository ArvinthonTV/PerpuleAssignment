package com.perpule.assignment.param;

/**
 * Created by 31250 on 5/24/2018.
 */
public class StoreCustomerInfoRequest {

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    private CustomerDetails customerDetails;
}
