package com.perpule.assignment;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class PerpuleApplication extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        application.sources(PerpuleApplication.class);
        return application;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(PerpuleApplication.class, args);
        }

}
