package com.perpule.assignment.controller;

import com.perpule.assignment.implementation.UserAuthService;
import com.perpule.assignment.param.ValidateLogonRequest;
import com.perpule.assignment.param.ValidateLogonResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authenticationService")
public class AuthenticationService {

    @RequestMapping(value="/userAuth",method = RequestMethod.POST)
    public ValidateLogonResponse userAuth(@RequestHeader("requestMode") String requestMode,
                                          @RequestBody ValidateLogonRequest validateLogonRequest){
        return UserAuthService.getInstance().validateUser(validateLogonRequest.getUsername(),
                validateLogonRequest.getPassword());
    }

}
