package com.perpule.assignment.implementation;

import com.perpule.assignment.param.CustomError;
import com.perpule.assignment.param.ValidateLogonResponse;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.perpule.assignment.param.ServiceConstants.LOGIN_ERROR_CODE;
import static com.perpule.assignment.param.ServiceConstants.LOGIN_ERROR_DEV_MESSAGE;
import static com.perpule.assignment.param.ServiceConstants.LOGIN_ERROR_MESSAGE;

/**validateUser
 * Created by 31250 on 5/25/2018.
 */
public class UserAuthServiceImpl implements UserAuthService {
    private static Map<String,String> userDetails = new HashMap<>();


   public UserAuthServiceImpl(){
       userDetails.put("Admin","Admin@123");
   }


    @Override
    public ValidateLogonResponse validateUser(String username, String password){
        SecurityServiceImpl securityServiceImpl = new SecurityServiceImpl();
        ValidateLogonResponse validateLogonResponse = new ValidateLogonResponse();
        CustomError error = new CustomError();
        if(this.userDetails.containsKey(username)){
            for(Map.Entry m: this.userDetails.entrySet()){
                if(m.getKey().equals(username)) {
                    if (m.getValue().equals(password)) {
                        validateLogonResponse.setLoginStatus(true);
                        validateLogonResponse.setAuthorizationResponseModel(
                                securityServiceImpl.generateToken(username));
                    }
                    else {
                        validateLogonResponse.setLoginStatus(false);
                        error.setCode(LOGIN_ERROR_CODE);
                        error.setMessage(LOGIN_ERROR_MESSAGE);
                        error.setDeveloperMessage(LOGIN_ERROR_DEV_MESSAGE);
                        validateLogonResponse.setError(error);
                    }
                }
            }
        }
        else {
            validateLogonResponse.setLoginStatus(false);
            error.setCode(LOGIN_ERROR_CODE);
            error.setMessage(LOGIN_ERROR_MESSAGE);
            error.setDeveloperMessage(LOGIN_ERROR_DEV_MESSAGE);
            validateLogonResponse.setError(error);
        }

        return validateLogonResponse;
    }
}
