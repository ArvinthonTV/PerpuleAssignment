package com.perpule.assignment.implementation;

import com.perpule.assignment.param.CustomerDetailsRequest;
import com.perpule.assignment.param.CustomerDetailsResponse;
import com.perpule.assignment.param.StoreCustomerInfoRequest;
import com.perpule.assignment.param.StoreCustomerInfoResponse;

public interface CustomerConfigDAO {

    public static CustomerConfigDAOImpl getInstance(){
        return new CustomerConfigDAOImpl();
    }

    public CustomerDetailsResponse getCustomerDetails(String customerKey) throws Exception;

    public StoreCustomerInfoResponse storeCustomerInformation(StoreCustomerInfoRequest storeCustomerInfoRequest) throws Exception;
}
