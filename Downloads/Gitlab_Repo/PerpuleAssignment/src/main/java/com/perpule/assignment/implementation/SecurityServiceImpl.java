package com.perpule.assignment.implementation;


import com.perpule.assignment.param.*;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.perpule.assignment.param.ServiceConstants.*;

@Service
public class SecurityServiceImpl implements SecurityService {

  @Override
  public AuthorizationResponseModel generateToken(String username) {

    //Local variable.
   AuthorizationResponseModel authorizationResponseModel = new AuthorizationResponseModel();

    //Generates the payload model.
    ClaimsModel claimsModel = new ClaimsModel(username);


    //Defines the expiration date for this token.
    Date expirationDate = new Date(System.currentTimeMillis() + ServiceConstants.EXPIRATION_TIME);

    //Generates the token string.
    String token = Jwts.builder().setClaims(claimsModel.toMap())
            .signWith(SignatureAlgorithm.HS512, ServiceConstants.SECRET.getBytes())
            .setIssuer(ServiceConstants.ISSUER)
            .setExpiration(expirationDate)
            .compact();

    //Creates an authorization model using the token string.
    authorizationResponseModel.setAuthorizationText(token);

    //Returns the authorization model.
    return authorizationResponseModel;
  }


  @Override
  public TokenResponseModel verifyToken(String authorizationText) {
    System.out.println("Inside verifyToken mtd: ");
    //Creates a tokenResponseModel using the provided authorizationText.
    TokenResponseModel tokenItem = new TokenResponseModel(authorizationText);

    //Declares a variable to hold the JWT's token claims.
    Claims claimsInstance;

    try {

      //Retrieves the claims contained inside the token.
      claimsInstance = Jwts.parser().setSigningKey(SECRET.getBytes())
              .parseClaimsJws(authorizationText.replace(TOKEN_PREFIX, ""))
              .getBody();
      System.out.println("Claims value: "+claimsInstance);

      //Verifies if the received token string comply with the business rules.
      verifyReceivedToken(tokenItem, claimsInstance);
    } catch (MalformedJwtException exception) {
      //Generates a token error using the parameters and it is added to the token instance.
      generatesTokenError(tokenItem, AUTHORISATION_ERROR_CODE, "authorizationText", exception.toString());
    } catch (ExpiredJwtException exception) {
      //Generates a token error using the parameters and it is added to the token instance.
      generatesTokenError(tokenItem, EXPIRATION_TIME_CODE, "expirationDate", exception.toString());
    } catch (RuntimeException exception) {
      //Generates a token error using the parameters and it is added to the token instance.
      generatesTokenError(tokenItem, UNSPECIFIED_ERROR_CODE, "unspecified", exception.toString());
    }

    //Returns an object with the verification process result.
    return tokenItem;
  }

  /*
   * Verifies if the different token's fields comply with the business rules.
   *
   * @param tokenItem      which will marked as okay or wrong if the verification fails.
   * @param claimsInstance contains the token's fields which will be verified.
   */

  private void verifyReceivedToken(TokenResponseModel tokenItem, Claims claimsInstance) {

    //Verifies the token's expiration date presence.
    verityExpirationDate(tokenItem, claimsInstance);
  }


  private void verityExpirationDate(TokenResponseModel tokenItem, Claims claimsInstance) {
    //Verifies if the tokenType is present at the claims map.
    Date expirationDate = claimsInstance.getExpiration();

    //Verifies if the expiration date is null, in this case the token was not build with an expiration date.
    if (expirationDate == null) {

      //Local variable
      String message = "The expirationDate field was not present at the authorization token. " +
              "Please request a new token.";

      //The verificationToken process was fail.
      tokenItem.setVerified(false);

      //Generates a token error using the parameters and it is added to the token instance.
      generatesTokenError(tokenItem, EXPIRATION_TIME_CODE, "expirationDate", message);

    } else {

      //The verificationToken process was okay.
      tokenItem.setVerified(true);
    }
  }

  /*
   * When the token verification process fails this method is used to mark the TokenResponseModel as failed. Also,
   * it creates a moreInfoItem with the details of the verification process errors.
   *
   * @param tokenItem is the instance which will be updated.
   * @param code      defines the http code which will be reported to the user.
   * @param parameter defines the inputParameter which has the problem.
   * @param message   defines the description text which will be reported to the user.
   */

  private static void generatesTokenError(TokenResponseModel tokenItem, int code, String parameter, String message) {

    //The verificationToken process was fail.
    tokenItem.setVerified(false);

    //Creates an additionalError instance with the received parameters.
    CustomError customError= new CustomError(code,parameter,message);
    customError.setCode(code);
    customError.setMessage(parameter);
    customError.setDeveloperMessage(message);

    List<CustomError> customErrorList = new ArrayList<>();
    customErrorList.add(customError);

    //Adds the additionalError instance to the tokenItem instance.
    tokenItem.setErrorList(customErrorList);
  }

}
