package com.perpule.assignment.param;

import java.util.HashMap;
import java.util.Map;

public class ClaimsModel {


  private String userId;

  /**
   * Construct an object with the received parameters.
   * @param userId
   */
  public ClaimsModel(String userId) {

    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  @Override
  public String toString() {
    return "SecurityRequestModel{" +
            "userId='" + userId + "'" +
            "}";
  }

  /**
   * Generates a map with the instance's fields keys and values.
   * @return
   */
  public Map<String, Object> toMap() {

    HashMap<String, Object> payloadMap = new HashMap<>();
    payloadMap.put("userId", userId);

    return payloadMap;
  }
}
