package com.perpule.assignment.config;

import com.perpule.assignment.param.ServiceConstants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSourceConfig {

    public Connection getConnection() throws Exception {
        Connection connection = getDBConnection();
        return connection;
    }


    /**
     * Method to obtain DataBase connection for offShore environment
     *
     * @return Connection
     * @throws SQLException
     */
    private Connection getDBConnection() throws SQLException{
        Connection connection = null;
        try {
                connection = DriverManager.getConnection(ServiceConstants.DB_URL, ServiceConstants.U_NAME,
                        ServiceConstants.P_WORD);

        } catch (SQLException e) {
            throw new SQLException(e);

        }
        return connection;
    }

}


