package com.perpule.assignment.implementation;

import com.perpule.assignment.param.AuthorizationResponseModel;
import com.perpule.assignment.param.TokenResponseModel;

/**
 * Created by 31250 on 5/25/2018.
 */
public interface SecurityService {

    /**
     * Generates a JWT Token using the specified parameters.
     *

     * @return AuthorizationResponseModel instance with the generated token.
     */
    //public AuthorizationResponseModel generateToken(String workstationId, SecurityRequestModel securityRequestModel);
    public AuthorizationResponseModel generateToken(String username);

    /**
     * Rerifies the received JWT Token.
     *
     * @param authorizationText holds the JWT token string.
     * @return a TokenResponseModel with the verification process information.
     */
    public TokenResponseModel verifyToken(String authorizationText);

    /**
     *Verifies if the tokenType, userId and passwordText fields complies with the expected requirements.
     * @param securityRequestModel
     * @return When the verification fails it returns a list of moreInfo elements with the error details.
     * Otherwise, when the process was okay, it returns a null value. Note, the null value represent a successful
     * verification.
     */
    // public Error verifySecurityRequestModel(SecurityRequestModel securityRequestModel);

}
