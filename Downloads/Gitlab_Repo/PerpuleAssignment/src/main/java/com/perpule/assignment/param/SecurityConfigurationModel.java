package com.perpule.assignment.param;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by efrain.salomon on 5/11/2018.
 */
public class SecurityConfigurationModel {

  /**
   * Contains the expirationTime specified at the Configuration source. It is used for generate the Token.
   * Note, its value is specified in milliseconds.
   */
  private Long expirationTime;

  /**
   * Contains the issuer string specified at the Configuration source. It is used for generate the Token.
   */
  private String issuer;

  /**
   * Class constructor
   ***********************************************************************/
  public SecurityConfigurationModel() {
    //SecurityConfigurationModel constructor
  }

  /**
   * Getters and setters
   *********************************************************************/
  public Long getExpirationTime() {
    return expirationTime;
  }

  public void setExpirationTime(Long expirationTime) {
    this.expirationTime = expirationTime;
  }

  public String getIssuer() {
    return issuer;
  }

  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

}
